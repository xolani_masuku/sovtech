import { useEffect, useState } from "react";

// I used this because it prevents child components to be called on serverside: check it out sovTech guys: https://www.joshwcomeau.com/react/the-perils-of-rehydration/
// it is just a wrapper

export default function ClientOnly({ children, ...delegated }) {
    const [hasMounted, setHasMounted] = useState(false);

    useEffect(() => {
        setHasMounted(true);
    }, []);

    if (!hasMounted) {
        return null;
    }

    return <>{children}</>;
}
