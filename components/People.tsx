import { gql, useQuery } from "@apollo/client";
import React, { useEffect, useState } from "react";
import styled from "styled-components";
import Person from "../models/Person";
import Response from "../models/Response";
import PersonView from "./PersonView";

const QUERY = gql`
    query People($offset: Int, $search: String) {
        people(offset: $offset, search: $search) {
            people {
                id
                name
                gender
                mass
                height
            }
            next
            previous
        }
    }
`;

export default function People() {
    const [offset, setOffset] = useState(1);
    const [search, setSearch] = useState("");

    const { data, loading, error, refetch } = useQuery(QUERY, {
        variables: { offset: 1, search: "" },
    });

    useEffect(() => {
        refetch({ offset: 1, search });
        setOffset(1);
    }, [search]);

    useEffect(() => {
        refetch({ offset, search });
    }, [offset]);

    if (!data && loading) {
        return <h2>Loading...</h2>;
    }

    if (error) {
        console.error(error);
        return null;
    }

    const response = data.people as Response;
    const people = response.people as Person[];
    const { previous, next } = response;

    return (
        <Container>
            <Input onChangeCapture={(e) => setSearch(e.target.value)} />
            {people.map((p) => (
                <PersonView key={p.id} data={p} />
            ))}

            <Pagination>
                {!!previous && (
                    <NavLink onClick={() => setOffset(+previous)}>Prev</NavLink>
                )}
                {!!next && (
                    <NavLink onClick={() => setOffset(+next)}>Next</NavLink>
                )}
            </Pagination>
        </Container>
    );
}

const Container = styled.div`
    width: 100%;
    margin-top: 30px;
`;
const Pagination = styled.div`
    display: flex;
    justify-content: flex-end;
    width: 100%;
`;
const NavLink = styled.div`
    padding: 10px;
    margin: 10px;
`;
const Input = styled.input``;
