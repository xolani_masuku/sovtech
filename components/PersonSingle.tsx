import { gql, useQuery } from "@apollo/client";
import React from "react";
import { useRouter } from "next/router";
import styled from "styled-components";
import Person from "../models/Person";

const QUERY = gql`
    query Person($id: Int!) {
        person(id: $id) {
            id
            name
            gender
            mass
            height
        }
    }
`;

export default function PersonSingle() {
    const router = useRouter();
    const { id } = router.query;
    const { data, loading, error } = useQuery(QUERY, {
        variables: { id: +id },
    });

    if (loading) {
        return <h2>Loading...</h2>;
    }

    if (error) {
        console.error(error);
        return null;
    }

    const person = data.person as Person;

    return (
        <Container>
            <div>name: {person.name}</div>
            <div>height: {person.height}</div>
            <div>mass: {person.mass}</div>
            <div>gender: {person.gender}</div>
        </Container>
    );
}

const Container = styled.div``;
