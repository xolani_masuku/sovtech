import React from "react";
import styled from "styled-components";
import Person from "../models/Person";
import Link from "next/link";

export default function PersonView({ data }) {
    const person = data as Person;

    return (
        <Link href={`/${person.id}`}>
            <Container>
                <div>name: {person.name}</div>
                <div>height: {person.height}</div>
                <div>mass: {person.mass}</div>
                <div>gender: {person.gender}</div>
            </Container>
        </Link>
    );
}

const Container = styled.div`
    border-bottom: 1px solid blue;
    padding-bottom: 10px;
    margin-bottom: 10px;
    cursor: pointer;
`;
