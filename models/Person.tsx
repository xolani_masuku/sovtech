export default class Person {
    id: number;
    url: string;
    name: string;
    height: string;
    mass: string;
    gender: string;
    homeworld: string;
}
