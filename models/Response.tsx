import Person from "./Person";

export default class Response {
    next: number;
    previous: number;
    people: Person[];
}
