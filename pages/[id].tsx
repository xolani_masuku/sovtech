import Head from "next/head";
import React from "react";
import ClientOnly from "../components/ClientOnly";
import PersonSingle from "../components/PersonSingle";
import styles from "../styles/Home.module.css";

export default function PersonPage() {
    return (
        <div className={styles.container}>
            <Head>
                <title>SovTech Project</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <main className={styles.main}>
                <ClientOnly>
                    <PersonSingle />
                </ClientOnly>
            </main>

            <footer className={styles.footer}>
                <a
                    href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    Powered by{" "}
                    <img
                        src="/vercel.svg"
                        alt="Vercel Logo"
                        className={styles.logo}
                    />
                </a>
            </footer>
        </div>
    );
}
