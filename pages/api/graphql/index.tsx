import { ApolloServer } from "apollo-server-micro";
import { resolvers } from "./resolvers";
import { typeDefs } from "./typeDefs";

const server = new ApolloServer({ resolvers, typeDefs });

export const config = {
    api: {
        bodyParser: false,
    },
};

export default server.createHandler({ path: "/api/graphql" });
