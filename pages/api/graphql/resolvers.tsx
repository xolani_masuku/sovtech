import fetch from "isomorphic-unfetch";
import Person from "../../../models/Person";
const BASE_URL = "https://swapi.dev/api/people/";

function extractID(url: string): number {
    return +url.replace(/[^0-9]/g, "");
}

export const resolvers = {
    Query: {
        people: async (_, args: { offset?: number; search?: string }) => {
            let query = `?page=${args.offset || 1}`;

            if (args.search) {
                query += `&search=${args.search}`;
            }

            const res = await fetch(`${BASE_URL}${query}`);
            const data = await res.json();
            const results = data.results as Person[];

            const people = results.map((p) => ({
                ...p,
                id: extractID(p.url),
            }));

            return {
                ...data,
                next: data.next ? extractID(data.next) : null,
                previous: data.previous ? extractID(data.previous) : null,
                people,
            };
        },
        person: async (_, args: { id: number }) => {
            const res = await fetch(`${BASE_URL}${args.id}/`);
            const data = (await res.json()) as Person;
            return { ...data, id: args.id };
        },
    },
};
