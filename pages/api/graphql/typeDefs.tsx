import { gql } from "apollo-server-micro";

export const typeDefs = gql`
    type Person {
        id: ID
        url: String
        name: String
        height: String
        mass: String
        gender: String
        homeworld: String
    }

    type Response {
        next: String
        previous: String
        people: [Person]
    }

    type Query {
        people(offset: Int, search: String): Response
        person(id: Int!): Person
    }
`;
